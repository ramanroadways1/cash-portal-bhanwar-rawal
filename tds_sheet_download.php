<?php 
require_once './connect.php';

$output ='';

$from = $_POST['from_date'];
$to = $_POST['to_date'];

$result = mysqli_query($conn,"SELECT freight_form.frno,freight_form.company,GROUP_CONCAT(freight_form_lr.lrno SEPARATOR ',') as lrno,newdate,
freight_form.truck_no,freight_form.branch,freight_form.actualf,freight_form.tds,ptob,pto_adv_name,adv_pan,adv_date FROM freight_form,freight_form_lr 
WHERE adv_date BETWEEN '$from' and '$to' and freight_form.tds>0 AND freight_form_lr.frno=freight_form.frno GROUP by freight_form.frno");

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($result) == 0)
{
	 echo "<script>
		alert('No result found..');
		window.location.href='./tds_sheet.php';
	</script>";
	exit();
}
	
$output .= '
	   <table border="1">  
		   <tr>  
               <th>FM_No</th>  
               <th>Company</th>  
               <th>LR_No</th>  
               <th>FM_Date</th>  
               <th>Truck_No</th>  
               <th>Branch</th>  
               <th>Freight</th>  
               <th>TDS</th>  
               <th>Adv_To</th>  
               <th>Adv_Party_Name</th>  
               <th>Adv_Party_PAN</th>  
               <th>Adv_Date</th>  
        </tr>';
		
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
    <tr> 
		<td>'.$row["frno"].'</td> 
		<td>'.$row["company"].'</td> 
		<td>'."'".$row["lrno"].'</td> 
		<td>'.$row["newdate"].'</td> 
		<td>'.$row["truck_no"].'</td> 
		<td>'.$row["branch"].'</td> 
		<td>'.$row["actualf"].'</td> 
		<td>'.$row["tds"].'</td> 
		<td>'.$row["ptob"].'</td> 
		<td>'.$row["pto_adv_name"].'</td> 
		<td>'.$row["adv_pan"].'</td> 
		<td>'.$row["adv_date"].'</td> 
	</tr>';
  }
  
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Tds_Sheet'.$from.'_To_'.$to.'.xls');
  echo $output;
?>