<?php
require_once("./connect.php");

$date=mysqli_real_escape_string($conn,$_POST['date']);
?>
<table class="table table-bordered" style="font-family:Verdana;font-size:13px">
				<tr>
					<th>Id</th>
					<th>Branch</th>
					<th>RRPL Cash Bal</th>
					<th>RR Cash Bal</th>
					<th>RRPL Wdl</th>
					<th>RR Wdl</th>
					<th>Total Bal</th>
					<th>Total Wdl</th>
					<th>DateTime</th>
				</tr>
				<?php
				$qry_wdl=mysqli_query($conn,"SELECT u.username as branch,u.balance as rrpl_bal,u.balance2 as rr_bal,SUM(u.balance+u.balance2) as total_bal_branch,
				SUM(c.credit) as rrpl_wd,SUM(c.credit2) as rr_wd,SUM(c.credit+c.credit2) as total_wdl,c.timestamp FROM user as u 
				LEFT OUTER JOIN cashbook as c ON c.date='$date' AND c.vou_type='CREDIT ADD BALANCE' AND c.user=u.username
				WHERE u.role='2' GROUP BY u.username ORDER BY u.username ASC");
				if(!$qry_wdl)
				{
					echo mysqli_error($conn);
					exit();
				}
				if(mysqli_num_rows($qry_wdl)>0)
				{
					$sn=1;
					while($row_wdl=mysqli_fetch_array($qry_wdl))
					{
					echo "<tr>
						<td>$sn</td>
						<td><a onclick=FetchBranchData('$row_wdl[branch]')>$row_wdl[branch]</a></td>
						<td>$row_wdl[rrpl_bal]</td>
						<td>$row_wdl[rr_bal]</td>
						<td>$row_wdl[rrpl_wd]</td>
						<td>$row_wdl[rr_wd]</td>
						<td>$row_wdl[total_bal_branch]</td>
						<td>$row_wdl[total_wdl]</td>
						<td>$row_wdl[timestamp]</td>
					</tr>";
					$sn++;
					}
				}
				else
				{
					echo "<tr>
						<td colspan='5'><b><font color='red'>No Records found..</font></b> </td>
						</tr>";
				}
				?>				
			</table>