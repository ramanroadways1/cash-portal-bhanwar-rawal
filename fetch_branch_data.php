<?php
require_once("./connect.php");

$today=date("Y-m-d");

$branch=mysqli_real_escape_string($conn,$_POST['branch']);
$date=mysqli_real_escape_string($conn,$_POST['date']);
// echo $branch." ".$date;
// exit();

$sum_truck=mysqli_query($conn,"SELECT SUM(amt) as total FROM mk_tdv WHERE date='$date' AND user='$branch'");
$sum_exp=mysqli_query($conn,"SELECT SUM(amt) as total FROM mk_venf WHERE date='$date' AND user='$branch'");
$sum_adv=mysqli_query($conn,"SELECT SUM(cashadv) as total FROM freight_form WHERE adv_date='$date' AND branch='$branch' AND cashadv>0");
$sum_bal=mysqli_query($conn,"SELECT SUM(paycash) as total FROM freight_form WHERE bal_date='$date' AND branch_bal='$branch' AND paycash>0");
if(!$sum_truck)
{
	echo mysqli_error($conn);exit();
}
if(!$sum_exp)
{
	echo mysqli_error($conn);exit();
}
if(!$sum_adv)
{
	echo mysqli_error($conn);exit();
}
if(!$sum_bal)
{
	echo mysqli_error($conn);exit();
}
$row_sum_truck=mysqli_fetch_array($sum_truck);
$row_sum_exp=mysqli_fetch_array($sum_exp);
$row_sum_adv=mysqli_fetch_array($sum_adv);
$row_sum_bal=mysqli_fetch_array($sum_bal);

$qry_exp_vou=mysqli_query($conn,"SELECT vno,comp,amt,des FROM mk_venf WHERE date='$date' AND user='$branch'");
if(!$qry_exp_vou)
{
	echo mysqli_error($conn);exit();
}
echo "
<h5 style='padding:5px;background:black;color:#FFF'>TruckVou : $row_sum_truck[total], ExpVou : $row_sum_exp[total], 
CashAdv : $row_sum_adv[total], CashBal : $row_sum_bal[total] ($date)</h5>

<h5 style='padding:5px;background:gray;color:#FFF'>ExpenseVou : $branch ($date)</h5>
<table class='table table-bordered' style='font-family:Verdana;font-size:12px'>
	<tr>
		<th>Id</th>
		<th>VouNo</th>
		<th>Comp</th>
		<th>Amount</th>
		<th>ExpDesc</th>
	</tr>	
		";
$i1=1;	
if(mysqli_num_rows($qry_exp_vou)>0)
{	
while($row_exp_vou=mysqli_fetch_array($qry_exp_vou))
{
	echo "<tr>
		<td>$i1</td>
		<td>
		<form action='view_voucher.php' target='_blank' method='POST'>
<input name='vou_no' value='$row_exp_vou[vno]' type='hidden' />
<input name='voutype' value='Expense_Voucher' type='hidden' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$row_exp_vou[vno]</a>
</form>
</td>
		<td>$row_exp_vou[comp]</td>
		<td>$row_exp_vou[amt]</td>
		<td>$row_exp_vou[des]</td>
		</tr>
		";
$i1++;
}
}
else
	{
		echo "<tr>
		<td colspan='5'><font color='red'><b>No expense vou found.</b></font></td>
		</tr>
		";
	}
echo "</table>";

$qry_truck_vou=mysqli_query($conn,"SELECT tdvid,company,amt,truckno,dname FROM mk_tdv WHERE date='$date' AND user='$branch'");
if(!$qry_truck_vou)
{
	echo mysqli_error($conn);exit();
}
echo "
<h5 style='padding:5px;background:gray;color:#FFF'>TruckVou : $branch ($date)</h5>
<table class='table table-bordered' style='font-family:Verdana;font-size:12px'>
	<tr>
		<th>Id</th>
		<th>VouNo</th>
		<th>Comp</th>
		<th>Amount</th>
		<th>TruckNo</th>
		<th>Driver</th>
	</tr>	
		";
$i2=1;	
if(mysqli_num_rows($qry_truck_vou)>0)
{	
while($row_truck_vou=mysqli_fetch_array($qry_truck_vou))
{
	echo "<tr>
		<td>$i2</td>
		<td>
		<form action='view_voucher.php' target='_blank' method='POST'>
<input name='vou_no' value='$row_truck_vou[tdvid]' type='hidden' />
<input name='voutype' value='Truck_Voucher' type='hidden' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$row_truck_vou[tdvid]</a>
</form>
</td>
		<td>$row_truck_vou[company]</td>
		<td>$row_truck_vou[amt]</td>
		<td>$row_truck_vou[truckno]</td>
		<td>$row_truck_vou[dname]</td>
		</tr>
		";
$i2++;
		}
}
else
{
	echo "<tr>
		<td colspan='6'><font color='red'><b>No truck vou found.</b></font></td>
		</tr>
		";
}
echo "</table>";

$qry_fm_adv=mysqli_query($conn,"SELECT frno,truck_no,totalf,cashadv FROM freight_form WHERE adv_date='$date' AND branch='$branch' AND cashadv>0");
if(!$qry_fm_adv)
{
	echo mysqli_error($conn);exit();
}
echo "
<h5 style='padding:5px;background:gray;color:#FFF'>FreightMemo Cash Adv : $branch ($date)</h5>
<table class='table table-bordered' style='font-family:Verdana;font-size:12px'>
	<tr>
		<th>Id</th>
		<th>FMNo</th>
		<th>TruckNo</th>
		<th>Freight</th>
		<th>CashAdv</th>
	</tr>	
		";
$i3=1;	
if(mysqli_num_rows($qry_fm_adv)>0)
{	
while($row_fm_adv=mysqli_fetch_array($qry_fm_adv))
{
	echo "<tr>
		<td>$i3</td>
		<td>
<form action='view_fm.php' target='_blank' method='POST'>
<input name='idmemo' value='$row_fm_adv[frno]' type='hidden' />
<input name='key' value='FM' type='hidden' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$row_fm_adv[frno]</a>
</form>
		</td>
		<td>$row_fm_adv[truck_no]</td>
		<td>$row_fm_adv[totalf]</td>
		<td>$row_fm_adv[cashadv]</td>
		</tr>
		";
$i3++;		
}
}
else
{
	echo "<tr>
		<td colspan='5'><font color='red'><b>No Fm Cash Adv found.</b></font></td>
		</tr>
		";
}
echo "</table>";


$qry_fm_bal=mysqli_query($conn,"SELECT frno,truck_no,totalbal,paycash FROM freight_form WHERE bal_date='$date' AND branch_bal='$branch' AND paycash>0");
if(!$qry_fm_bal)
{
	echo mysqli_error($conn);exit();
}
echo "
<h5 style='padding:5px;background:gray;color:#FFF'>FreightMemo Cash Bal : $branch ($date)</h5>
<table class='table table-bordered' style='font-family:Verdana;font-size:12px'>
	<tr>
		<th>Id</th>
		<th>FMNo</th>
		<th>TruckNo</th>
		<th>BalFreight</th>
		<th>CashBal</th>
	</tr>	
		";
$i4=1;	
if(mysqli_num_rows($qry_fm_bal)>0)
{	
while($row_fm_bal=mysqli_fetch_array($qry_fm_bal))
{
	echo "<tr>
		<td>$i4</td>
		<td>
<form action='view_fm.php' target='_blank' method='POST'>
<input name='idmemo' value='$row_fm_bal[frno]' type='hidden' />
<input name='key' value='FM' type='hidden' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$row_fm_bal[frno]</a>
</form>
		</td>
		<td>$row_fm_bal[truck_no]</td>
		<td>$row_fm_bal[totalbal]</td>
		<td>$row_fm_bal[paycash]</td>
		</tr>
		";
$i4++;
		}
}
else
{
	echo "<tr>
		<td colspan='5'><font color='red'><b>No FM Cash Balance found.</b></font></td>
		</tr>
		";
}
echo "</table>";
?>