<?php
require_once('connect.php');

$vou_no=mysqli_real_escape_string($conn,strtoupper($_POST['vou_no']));
$voutype=mysqli_real_escape_string($conn,$_POST['voutype']);
?>		
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css" media="print">
@media print {
body {
   zoom:70%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.printpage * { visibility: visible}
.head * { visibility: visible}
.fileupload * { display: none}
.buttons * { display: none}
.printpage { position: absolute; top: 0; left: 0;}
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>

<body style="overflow-x:hidden;">
<div class="printpage">
<?php 
if($voutype=='Expense_Voucher')
{
$sql="select * from mk_venf where vno='$vou_no'";

if($result = mysqli_query($conn,$sql))
{
if(mysqli_num_rows($result) > 0)
    {
$row = mysqli_fetch_array($result);
$vou_date = date("d-m-Y", strtotime($row['newdate']));
if($row['comp']=='RRPL')
{
	$logo='<img src="logo/rrpl.jpg" style="width:500px" />';
}
else
{
	$logo='<img src="logo/rr.jpg" style="width:500px" />';
}

?>
<br />
<br />
<div class="container">
<center>
<table style="width:1100px;height:500px;font-family:Verdana;font-size:15px;" class="table table-bordered">
     <tr>
       <td colspan="2"><?php echo $logo; ?></td>
       <td colspan="2"><center><b>Vou No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row ['user']."</center>"; ?></b></td>
      </tr>
	   <tr>
	   <th>Expense Desc. </th>
        <td style="font-size:16px;" id="linew"><?php echo $row ['des']; ?></td>
        <th>Vou. Date </th>
        <td id="linew"><?php echo $vou_date; ?></td>
	 </tr>
     <tr>
	 <th>Amount (in words)</th>
        <td id="linew"><?php echo $row ['amt_w']; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row ['amt']; ?></td>
		
      </tr>
      <tr>
        <th>Payment By </th>
        <td id="linew"><?php echo $row ['chq']; ?></td>
		<th>PAN No. </th>
        <td id="linew"><?php echo $row ['pan']; ?></td>
      </tr>
	  <?php
	  if($row['chq']=='CHEQUE')
	  {
	 ?>
	  <tr>
        <th>Cheque No.  </th>
        <td id="linew"><?php echo $row ['chq_no']; ?></td>
        <th>Bank Name  </th>
        <td id="linew"><?php echo $row ['chq_bnk_n']; ?></td>
      </tr>
	 <?php
	  }
	  else if($row['chq']=='NEFT')
	  {
	 ?>
	  <tr>
        <th>A/c Holder </th>
        <td id="linew"><?php echo $row ['neft_acname']; ?></td>
        <th>A/c No </th>
        <td id="linew"><?php echo $row ['neft_acno']; ?></td>
      </tr>
	   <tr>
        <th>Bank Name </th>
        <td id="linew"><?php echo $row ['neft_bank']; ?></td>
        <th>IFSC Code </th>
        <td id="linew"><?php echo $row ['neft_ifsc']; ?></td>
      </tr>
	  <?php 
	  }
	  ?>
     
      <tr>
		<th>Narration </th>
        <td id="linew"><?php echo $row ['narrat']; ?></td>
        <th class="fileupload">Uploads</th>
        <td id="linew" class="fileupload">
		<form action="exp_vou_download.php" method="POST" target="_blank">
		<input type="hidden" name="vno" value="<?php echo $vou_no; ?>" />
		<input type="submit" value="View Attachment File" class="btn btn-sm btn-primary" />
		</form>
		</td>
      </tr>
    <?php
	if($row ['amt']>=5000)
	{	
	?>
	<tr>
       <th style="height:100px;">Cashier Sign</th>
	   <th style="height:100px;">Accountant Sign</th>
	   <th style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th style="height:100px;">Cashier Sign</th>
	   <th style="height:100px;">Accountant Sign</th>
	   <th style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>
</div>

<center>
<br />

<button onclick="print();" style="font-family:Verdana;letter-spacing:1px;color:#000" class="btn-md btn btn-warning"><b>Print Voucher</button></b>
&nbsp;
<a href="./"><button style="font-family:Verdana;letter-spacing:1px;color:#000" class="btn btn-warning btn-md"><b>Dashboard</button></b></a>
</center>

<?php

}
else
{
	echo "<script>
		alert('No result found...');
		window.location.href='./';
		window.close();
		</script>";
		mysqli_close($conn);
		exit();
}
}
}
?>
<div class="printpage">
<?php
if($voutype=='Truck_Voucher')
{
$sql="select * from mk_tdv where tdvid='$vou_no' order by id desc limit 1";

if($result = mysqli_query($conn,$sql))
{
if(mysqli_num_rows($result) > 0)
    {
$row = mysqli_fetch_array($result);
$vou_date = date("d-m-Y", strtotime($row['newdate']));
if($row['company']=='RRPL')
{
	$logo='<img src="logo/rrpl.jpg" style="width:500px" />';
}
else
{
	$logo='<img src="logo/rr.jpg" style="width:500px" />';
}

?>
<br />
<br />
<div class="container">
<center>
<table class="table table-bordered" style="width:1100px;font-family:Verdana;font-size:15px;height:500px">
      <tr>
       <td colspan="2"><?php echo $logo; ?></td>
       <td colspan="2"><center><b>Vou No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row ['user']."</center>"; ?></b></td>
      </tr>
	  <tr>
		<th>Truck No.</th>
        <td style="font-size:20px;" id="linew"><?php echo $row ['truckno']; ?></td>
        <th>Vou. Date  </th>
        <td id="linew"><?php echo $vou_date; ?></td>
	</tr>
      <tr>
		 <th>Amount (words)</th>
        <td id="linew"><?php echo $row ['amtw']; ?></td>
		<th>Amount  </th>
        <td id="linew"><?php echo $row ['amt']; ?></td>
      </tr>
      <tr>
        <th>Narration/Dest.</th>
        <td colspan="3" id="linew"><?php echo $row ['dest']; ?></td>
		</tr>
      <tr>
		<th>Driver Name</th>
        <td id="linew"><?php echo $row ['dname']; ?></td>
		<th>PAN No. </th>
        <td id="linew"><?php echo $row ['pan']; ?></td>
      </tr>
	  	  <?php
	  if($row['mode']=='CHEQUE')
	  {
	 ?>
	  <tr>
        <th>Cheque No.  </th>
        <td id="linew"><?php echo $row ['chq_no']; ?></td>
        <th>Bank Name  </th>
        <td id="linew"><?php echo $row ['chq_bank']; ?></td>
      </tr>
	 <?php
	  }
	  else if($row['mode']=='NEFT')
	  {
	 ?>
	  <tr>
        <th>A/c Holder </th>
        <td id="linew"><?php echo $row ['ac_name']; ?></td>
        <th>A/c No </th>
        <td id="linew"><?php echo $row ['ac_no']; ?></td>
      </tr>
	   <tr>
        <th>Bank Name </th>
        <td id="linew"><?php echo $row ['bank']; ?></td>
        <th>IFSC Code </th>
        <td id="linew"><?php echo $row ['ifsc']; ?></td>
      </tr>
	  <?php 
	  }
	  ?>
     
    <?php
	if($row ['amt']>=5000)
	{	
	?>
	<tr>
       <th style="height:100px;">Cashier Sign</th>
	   <th style="height:100px;">Accountant Sign</th>
	   <th style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th style="height:100px;">Cashier Sign</th>
	   <th style="height:100px;">Accountant Sign</th>
	   <th style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
    </tbody>
</table>
</center>
</div>
</div>
<center>
<br />
<div class="buttons">
<button style="font-family:Verdana;letter-spacing:1px;color:#000;" onclick="print();" class="btn btn-warning">Print Voucher</button>
&nbsp;
<a href="./"><button style="font-family:Verdana;letter-spacing:1px;color:#000;" class="btn btn-warning">Dashboard</button></a>
</center>
</div>



<?php 
}
else
{
	echo "<script>
		alert('No result found...');
		window.location.href='./';
		window.close();
		</script>";
		mysqli_close($conn);
		exit();
}
}
}
?>
<script>
function myFunction() {
    window.print();
}
</script>
</body>
</html>					
<?php
mysqli_close($conn);
?>	