<?php
require_once 'connect.php';
$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASH PORTAL : RAMAN ROADWAYS PVT LTD</title>
<meta http-equiv="refresh" content="240">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

<style> 
 label{
	 font-family:Verdana;
	 font-size:14px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 

</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	<br />
	<div class="row">
	
		<div class="form-group col-md-10 col-md-offset-1">
		<br />
		<br />
		<br />
			<div class="col-md-3">
			<form action="attd_download.php" target="_blank" method="POST" autocomplete="off">	
                         <div class="form-group">
                            <label class="control-label mb-1">Month <font color="red"><sup>*</sup></font></label>
                                                        <select class="form-control" name="month" required>
															<option value="">Select an option</option>
															<option value="1">JAN</option>
															<option value="2">FEB</option>
															<option value="3">MARCH</option>
															<option value="4">APRIL</option>
															<option value="5">MAY</option>
															<option value="6">JUNE</option>
															<option value="7">JULY</option>
															<option value="8">AUG</option>
															<option value="9">SEP</option>
															<option value="10">OCT</option>
															<option value="11">NOV</option>
															<option value="12">DEC</option>
														</select>
                                                    </div>
                                                </div>
												
												<div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-1">Year <font color="red"><sup>*</sup></font></label>
                                                        <select class="form-control" name="year" required>
															<option value="">Select an option</option>
															<option value="2017">2017</option>
															<option value="2018">2018</option>
															<option value="2019">2019</option>
															<option value="2020">2020</option>
															<option value="2021">2021</option>
															<option value="2022">2022</option>
															<option value="2023">2023</option>
															<option value="2024">2024</option>
															<option value="2025">2025</option>
															<option value="2026">2026</option>
															<option value="2027">2027</option>
															<option value="2028">2028</option>
														</select>
                                                    </div>
                                                </div>
												
												<div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-1">Branch Name <font color="red"><sup>*</sup></font></label>
                                                        <select class="form-control" name="branch" required>
															<option value="">Select an option</option>
															<option value="ALL">ALL BRANCHES</option>
															<?php
															$fetch_branch=mysqli_query($conn,"SELECT username FROM user WHERE role='2' ORDER BY username ASC");
															if(!$fetch_branch)
															{
																echo mysqli_error($conn_db);exit();
															}
															if(mysqli_num_rows($fetch_branch)>0)
															{
																while($row_branch=mysqli_fetch_array($fetch_branch))
																{
																	echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
																}
															}
															?>
														</select>
                                                    </div>
                                                </div>
			  
			<div class="col-md-12">
                   <div class="form-group">
                     <button type="submit" id="button1" class="btn btn-danger">Fetch Records !</button>
                  </div>
               </div>
		</div>
		</div>
		
</div>
<br />
</div>
</div>

</body>
</html>