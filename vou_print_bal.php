<?php
require_once('connect.php');

$vou_no=mysqli_real_escape_string($conn,strtoupper($_POST['frno']));
?>		
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css" media="print">
@media print {
body {
   zoom:45%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.printpage * { visibility: visible}
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>

<body style="overflow-x:hidden;">

<?php
$ones = array(
 "",
 " ONE",
 " TWO",
 " THREE",
 " FOUR",
 " FIVE",
 " SIX",
 " SEVEN",
 " WEIGHT",
 " NINE",
 " TEN",
 " ELEVEN",
 " TWELVE",
 " THIRTEEN",
 " FOURTEEN",
 " FIFTEEN",
 " SIXTEEN",
 " SEVENTEEN",
 " EIGHTEEN",
 " NINETEEN"
);
 
$tens = array(
 "",
 "",
 " TWENTY",
 " THIRTY",
 " FORTY",
 " FIFTY",
 " SIXTY",
 " SEVENTY",
 " EIGHTY",
 " NINETY"
);
 
$triplets = array(
 "",
 " THOUSAND",
 " MILLION",
 " BILLION",
 " TRILLION",
 " quadrillion",
 " quintillion",
 " sextillion",
 " septillion",
 " octillion",
 " nonillion"
);
 
 // recursive fn, converts three digits per pass
function convertTri($num, $tri) {
  global $ones, $tens, $triplets;
 
  // chunk the number, ...rxyy
  $r = (int) ($num / 1000);
  $x = ($num / 100) % 10;
  $y = $num % 100;
 
  // init the output string
  $str = "";
 
  // do hundreds
  if ($x > 0)
   $str = $ones[$x] . " HUNDRED";
 
  // do ones and tens
  if ($y < 20)
   $str .= $ones[$y];
  else
   $str .= $tens[(int) ($y / 10)] . $ones[$y % 10];
 
  // add triplet modifier only if there
  // is some output to be modified...
  if ($str != "")
   $str .= $triplets[$tri];
 
  // continue recursing?
  if ($r > 0)
   return convertTri($r, $tri+1).$str;
  else
   return $str;
 }
 
// returns the number as an anglicized string
function convertNum($num) {
 $num = (int) $num;    // make sure it's an integer
 
 if ($num < 0)
  return "NEGATIVE".convertTri(-$num, 0);
 
 if ($num == 0)
  return "ZERO";
 
 return convertTri($num, 0);
}
 
 // Returns an integer in -10^9 .. 10^9
 // with log distribution
 function makeLogRand() {
  $sign = mt_rand(0,1)*2 - 1;
  $val = randThousand() * 1000000
   + randThousand() * 1000
   + randThousand();
  $scale = mt_rand(-9,0);
 
  return $sign * (int) ($val * pow(10.0, $scale));
 }
 
$qry=mysqli_query($conn,"SELECT frno,company,branch,truck_no,actualf,baladv,unloadd,otherfr,claim,late_pod,totalbal,bal_date,pto_bal_name,
bal_pan,paycash,paydsl,newrtgsamt FROM freight_form WHERE frno='$vou_no'");

if(mysqli_num_rows($qry)==0)
{
	echo "Zero Result.";
	exit();
}

$row=mysqli_fetch_array($qry);

if($row['company']=='RRPL')
{
	$logo='<img src="logo/rrpl.jpg" style="width:500px" />';
}
else
{
	$logo='<img src="logo/rr.jpg" style="width:500px" />';
}

$qry_lrno=mysqli_query($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$vou_no'");

$qry_lr_all=mysqli_query($conn,"SELECT SUM(weight) as total_weight,date,consignor,fstation,tstation FROM freight_form_lr WHERE frno='$vou_no'");

$row_sum=mysqli_fetch_array($qry_lr_all);

$lrnos=array();

while($row_lr=mysqli_fetch_array($qry_lrno))
{
	$lrnos[] = $row_lr['lrno'];	
}
	
	$lrnos = implode(',', $lrnos);
 
?>
<div class="printpage">
<?php 
if($row['paycash']>0)
{
?>
<br />
<br />
<div class="container-fluid">
<center>
<table style="width:1250px;height:500px;font-family:Verdana;font-size:16px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $row_sum['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row_sum['date'])); ?></td>
		<th>Bal Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">CASH</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['paycash'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['paycash']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row_sum['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row_sum['total_weight'])." pmt"; ?></td>
      </tr>

		<tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row_sum['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row_sum['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['paycash']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>

<?php
}
?>

<!-- DIESEL --> 

<?php
if($row['paydsl']>0)
{
?>
<hr style="border-top: dotted 1px;" />
<div class="container-fluid">
<center>
<table style="width:1250px;height:500px;font-family:Verdana;font-size:16px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $row_sum['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row_sum['date'])); ?></td>
		<th>Adv Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">DIESEL</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['paydsl'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['paydsl']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row_sum['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row_sum['total_weight'])." pmt"; ?></td>
      </tr>
	  
	  	<tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row_sum['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row_sum['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['paydsl']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>

<?php
}
?>

<!-- RTGS -->

<?php
if($row['newrtgsamt']>0)
{
?>
<hr style="border-bottom:dotted 1px;" />

<div class="container-fluid">
<center>
<table style="width:1250px;height:500px;font-family:Verdana;font-size:16px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $row_sum['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row_sum['date'])); ?></td>
		<th>Adv Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">RTGS</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['newrtgsamt'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['newrtgsamt']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row_sum['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row_sum['total_weight'])." pmt"; ?></td>
      </tr>
	  
	  <tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:16px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row_sum['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row_sum['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['newrtgsamt']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>

<?php
}
?>
</div>
<br>
<br>
<center>
<button id="button1" onclick="print();" style="font-family:Verdana;letter-spacing:1px;color:#000" class="btn-md btn btn-warning"><b>Print Voucher</button></b>
</center>
<br>
<br>

<script>
function myFunction() {
    window.print();
}
</script>
</body>
</html>					
<?php
mysqli_close($conn);
?>	