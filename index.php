<?php
require_once 'connect.php';
$today=date('Y-m-d');

$today_data=mysqli_query($conn,"SELECT SUM(truck_vou) as truck_vou,SUM(truck_vou_amount) as truck_vou_amount,SUM(exp_vou) as exp_vou,
SUM(exp_vou_amount) as exp_vou_amount, SUM(fm_adv) as fm_adv,SUM(fm_adv_amount) as fm_adv_amount,SUM(fm_bal) as fm_bal,
SUM(fm_bal_amount) as fm_bal_amount,SUM(diesel_qty_market) as diesel_qty_market,SUM(diesel_amount_market) as diesel_amount_market,
SUM(diesel_qty_own) as diesel_qty_own,SUM(diesel_amount_own) as diesel_amount_own,SUM(neft) as neft,SUM(neft_amount) as neft_amount 
FROM today_data WHERE date='$today'");
 if(!$today_data)
 {
	 echo mysqli_error($conn);
	 exit();
 }
 $row_today=mysqli_fetch_array($today_data);
 
 $today_data2=mysqli_query($conn,"SELECT SUM(debit+debit2) as cash FROM cashbook WHERE date='$today'");
  if(!$today_data2)
 {
	 echo mysqli_error($conn);
	 exit();
 }
 $row_cash=mysqli_fetch_array($today_data2);
 
 $today_data3=mysqli_query($conn,"SELECT SUM(amount) as cheque FROM cheque_book WHERE date='$today'");
  if(!$today_data3)
 {
	 echo mysqli_error($conn);
	 exit();
 }
 $row_cheque=mysqli_fetch_array($today_data3);
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASH PORTAL : RAMAN ROADWAYS PVT LTD</title>
<meta http-equiv="refresh" content="240">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

<style> 
 label{
	 font-family:Verdana;
	 font-size:12px;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 

</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	<br />
	<div class="row">
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_cash['cash']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							Cash(RRPL+RR) Today</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_cheque['cheque']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							Cheque Today</div>
						</div>
					</div>
				</div>
			</div>	

			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['diesel_amount_market']+$row_today['diesel_amount_own']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;"><b><?php echo $row_today['diesel_qty_market']+$row_today['diesel_qty_own']; ?>
							</b> Diesel Today</div>
						</div>
					</div>
				</div>
			</div>	  

		<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['neft_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;"><b><?php echo $row_today['neft']; ?>
							</b> NEFT/RTGS Today</div>
						</div>
					</div>
				</div>
			</div>	  			
</div>
<br />

	<div class="row">
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['truck_vou_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_today['truck_vou']; ?></b>
							TruckVou Today</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['exp_vou_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_today['exp_vou']; ?></b>
							ExpVou Today</div>
						</div>
					</div>
				</div>
			</div>	

			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['fm_adv_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_today['fm_adv']; ?></b>
							FM Adv Today</div>
						</div>
					</div>
				</div>
			</div>	  

		<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_today['fm_bal_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_today['fm_bal']; ?></b>
							FM Bal Today</div>
						</div>
					</div>
				</div>
			</div>	  			
</div>

<br />

<?php
$qry_wdl=mysqli_query($conn,"SELECT SUM(credit+credit2) as withdrawal,COUNT(id) as total_wdl FROM cashbook WHERE date='$today' AND vou_type='CREDIT ADD BALANCE'");
if(!$qry_wdl)
{
	echo mysqli_error($conn);
	exit();
}
$row_wdl=mysqli_fetch_array($qry_wdl);

$lr_age=mysqli_query($conn,"SELECT lr_30 FROM lr_old WHERE date='$today'");
if(!$lr_age)
{
	echo mysqli_error($conn);
	exit();
}
$row_lr_30=mysqli_fetch_array($lr_age);
$lr_age_30=$row_lr_30['lr_30'];

$qry_ho=mysqli_query($conn,"SELECT SUM(credit+credit2+debit+debit2) as ho,COUNT(id) as total_ho FROM cashbook WHERE date='$today' AND 
vou_type IN('CREDIT-HO','DEBIT-HO')");
if(!$qry_ho)
{
	echo mysqli_error($conn);
	exit();
}
$row_ho=mysqli_fetch_array($qry_ho);

$qry_dispatch=mysqli_query($conn,"SELECT id FROM lr_sample WHERE date='$today'");
if(!$qry_dispatch)
{
	echo mysqli_error($conn);
	exit();
}
$row_dispatch=mysqli_fetch_array($qry_dispatch);
?>

	<div class="row">
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_wdl['withdrawal']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_wdl['total_wdl']; ?></b>
							Withdrawal Today</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;"><?php echo $lr_age_30; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							LR Age > 30days</div>
						</div>
					</div>
				</div>
			</div>	

			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $row_ho['ho']; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $row_ho['total_ho']; ?></b>
							HO - Dr/Cr</div>
						</div>
					</div>
				</div>
			</div>	  

		<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;"><?php echo mysqli_num_rows($qry_dispatch); ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							
							Dispatch Today</div>
						</div>
					</div>
				</div>
			</div>	  			
</div>

<div class="row" style="border-bottom:0px solid #ddd;border-top:0px solid #ddd;font-family:Verdana;color:#000;">

	<div class="form-group col-md-3 col-sm-12">
		<form method="post" action="view_vou.php" autocomplete="off" target="_blank">
					<label>Truck Vou Id <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="vou_no" required />
			   <input value="Truck_Voucher" type="hidden" name="voutype" />
			   <button type="submit" name="submit" class="btn btn-block btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	 
	 <div class="form-group col-md-3 col-sm-12">
		<form method="post" action="view_vou.php" autocomplete="off" target="_blank">
					<label>Exp Vou Id <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="vou_no" required />
			   <input value="Expense_Voucher" type="hidden" name="voutype" />
			   <button type="submit" name="submit" class="btn btn-block btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	 
	 <div class="form-group col-md-3 col-sm-12">
		<form method="post" action="view_fm.php" autocomplete="off" target="_blank">
					<label>FM By Id <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="idmemo" required />
			   <input value="FM" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-block btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	
	<div class="form-group col-md-3 col-sm-12">
		<form method="post" action="view_fm.php" autocomplete="off" target="_blank">
					<label>FM By LR No <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="lrno" required />
			   <input value="LR" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-sm btn-block btn-danger">Show Vou</button>
		</form>
     </div>
	 
      </div>
<br />

<!-- SUMMARY CASH TABLE STARTS -->

<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:#555;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i><b>RRPL</b> Cashbook </b>Summary (today)</i></div>
	<div class="panel-body" style="overflow-x:hidden;">
<?php
$rrpl_cash=mysqli_query($conn,"SELECT user,vou_no,vou_type,debit,credit FROM cashbook WHERE date='$today' AND comp='RRPL' ORDER BY user 
ASC,id ASC");
if(!$rrpl_cash)
{
	echo mysqli_error($conn);
	exit();
}
echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
			<th>Branch</th>
			<th>Vou No</th>
			<th>Vou Type</th>
			<th>Dr</th>
			<th>Cr</th>
		</tr>";

while($row_rrpl=mysqli_fetch_array($rrpl_cash))
  {
echo      "<tr>
				<td>$row_rrpl[user]</td>
				<td>$row_rrpl[vou_no]</td>
				<td>$row_rrpl[vou_type]</td>
				<td>$row_rrpl[debit]</td>
				<td>$row_rrpl[credit]</td>
		</tr>";

}
echo  "</table>";
?>
				</div>
			</div>
		</div>
		
		<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:#555;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i><b>RR</b> Cashbook </b>Summary (today)</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$rr_cash=mysqli_query($conn,"SELECT user,vou_no,vou_type,debit2,credit2 FROM cashbook WHERE date='$today' AND comp='RAMAN_ROADWAYS' ORDER BY user 
ASC,id ASC");
if(!$rr_cash)
{
	echo mysqli_error($conn);
	exit();
}

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
			<th>Branch</th>
			<th>Vou No</th>
			<th>Vou Type</th>
			<th>Dr</th>
			<th>Cr</th>
		</tr>";

while($row_rr=mysqli_fetch_array($rr_cash))
  {
echo      "<tr>
				<td>$row_rr[user]</td>
				<td>$row_rr[vou_no]</td>
				<td>$row_rr[vou_type]</td>
				<td>$row_rr[debit2]</td>
				<td>$row_rr[credit2]</td>
		</tr>";

}
echo  "</table>";
?>

					</div>
				</div>
			</div>
		</div>
		
	<!-- SUMMARY CASH TABLE ENDS -->	
		
		<br />
		
<!-- SUMMARY BANK TABLE STARTS -->

<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:#555;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i><b>RRPL</b> Passbook </b>Summary (today)</i></div>
	<div class="panel-body" style="overflow-x:hidden;">
<?php
$rrpl_pass=mysqli_query($conn,"SELECT user,vou_no,vou_type,debit,credit FROM passbook WHERE date='$today' AND comp='RRPL' ORDER BY user 
ASC,id ASC");
if(!$rrpl_pass)
{
	echo mysqli_error($conn);
	exit();
}
echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
			<th>Branch</th>
			<th>Vou No</th>
			<th>Vou Type</th>
			<th>Dr</th>
			<th>Cr</th>
		</tr>";

while($row_rrpl_pass=mysqli_fetch_array($rrpl_pass))
  {
echo      "<tr>
				<td>$row_rrpl_pass[user]</td>
				<td>$row_rrpl_pass[vou_no]</td>
				<td>$row_rrpl_pass[vou_type]</td>
				<td>$row_rrpl_pass[debit]</td>
				<td>$row_rrpl_pass[credit]</td>
		</tr>";

}
echo  "</table>";
?>
				</div>
			</div>
		</div>
		
		<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:#555;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i><b>RR</b> Passbook </b>Summary (today)</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$rr_pass=mysqli_query($conn,"SELECT user,vou_no,vou_type,debit2,credit2 FROM passbook WHERE date='$today' AND comp='RAMAN_ROADWAYS' ORDER BY user 
ASC,id ASC");
if(!$rr_pass)
{
	echo mysqli_error($conn);
	exit();
}

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
			<th>Branch</th>
			<th>Vou No</th>
			<th>Vou Type</th>
			<th>Dr</th>
			<th>Cr</th>
		</tr>";

while($row_rr_pass=mysqli_fetch_array($rr_pass))
  {
echo      "<tr>
				<td>$row_rr_pass[user]</td>
				<td>$row_rr_pass[vou_no]</td>
				<td>$row_rr_pass[vou_type]</td>
				<td>$row_rr_pass[debit2]</td>
				<td>$row_rr_pass[credit2]</td>
		</tr>";

}
echo  "</table>";
?>

					</div>
				</div>
			</div>
		</div>
		
	<!-- SUMMARY BANK TABLE ENDS -->	
		
</div>
</div>

</body>
</html>