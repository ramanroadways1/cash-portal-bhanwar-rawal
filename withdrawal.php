<?php
require_once("./connect.php");
$today=date("Y-m-d");
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASH PORTAL : RAMAN ROADWAYS PVT LTD</title>
<meta http-equiv="refresh" content="240">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

<style> 
 label{
	 font-family:Verdana;
	 font-size:12px;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 

</head>

<script>
function FetchWdl(date)
{
						$("#loadicon").show();
						jQuery.ajax({
						url: "fetch_withdrawal_data.php",
						data: 'date=' + date,
						type: "POST",
						success: function(data) {
						$("#wdl_data").html(data);
						$("#loadicon").hide();
						$("#date").val(date);
						},
						error: function() {}
						});
						return true;
}
</script>

<a href="./"><button style="margin:10px" class="btn btn-danger">Dashboard</button></a>
<input type="hidden" value="<?php echo $today; ?>" id="date">
<div class="container-fluid" style="font-family:Verdana">
	<div class="row">
		<div class="form-group col-md-10">
		<input type="date" style="width:50%" class="form-control" max="<?php echo $today; ?>" onchange="FetchWdl(this.value)" />
			<h4 style="padding:5px;background:gray;color:#FFF">Cash & Withdrawal Summary:</h4>
			<div id="wdl_data" class="table-responsive" style="height:300px;overflow:auto">
			<table class="table table-bordered" style="font-family:Verdana;font-size:13px">
				<tr>
					<th>Id</th>
					<th>Branch</th>
					<th>RRPL Cash Bal</th>
					<th>RR Cash Bal</th>
					<th>RRPL Wdl</th>
					<th>RR Wdl</th>
					<th>Total Bal</th>
					<th>Total Wdl</th>
					<th>DateTime</th>
				</tr>
				<?php
				$qry_wdl=mysqli_query($conn,"SELECT u.username as branch,u.balance as rrpl_bal,u.balance2 as rr_bal,SUM(u.balance+u.balance2) as total_bal_branch,
				SUM(c.credit) as rrpl_wd,SUM(c.credit2) as rr_wd,SUM(c.credit+c.credit2) as total_wdl,c.timestamp FROM user as u 
				LEFT OUTER JOIN cashbook as c ON c.date='$today' AND c.vou_type='CREDIT ADD BALANCE' AND c.user=u.username
				WHERE u.role='2' GROUP BY u.username ORDER BY u.username ASC");
				if(!$qry_wdl)
				{
					echo mysqli_error($conn);
					exit();
				}
				if(mysqli_num_rows($qry_wdl)>0)
				{
					$sn=1;
					while($row_wdl=mysqli_fetch_array($qry_wdl))
					{
					//<td><a onclick=FetchBranchData('$row_wdl[user]')>$row_wdl[user]</a></td>	
					echo "<tr>
						<td>$sn</td>
						<td><a onclick=FetchBranchData('$row_wdl[branch]')>$row_wdl[branch]</a></td>
						<td>$row_wdl[rrpl_bal]</td>
						<td>$row_wdl[rr_bal]</td>
						<td>$row_wdl[rrpl_wd]</td>
						<td>$row_wdl[rr_wd]</td>
						<td>$row_wdl[total_bal_branch]</td>
						<td>$row_wdl[total_wdl]</td>
						<td>$row_wdl[timestamp]</td>
					</tr>";
					$sn++;
					}
				}
				else
				{
					echo "<tr>
						<td colspan='5'><b><font color='red'>No Records found..</font></b> </td>
						</tr>";
				}
				?>				
			</table>
			</div>
			
		</div>
		
		<script>
		function FetchBranchData(branch)
		{
						var date = $('#date').val();
						$("#loadicon").show();
						jQuery.ajax({
						url: "fetch_branch_data.php",
						data: 'branch=' + branch + '&date=' + date,
						type: "POST",
						success: function(data) {
						$("#branch_data").html(data);
						$("#loadicon").hide();
						},
						error: function() {}
						});
						return true;
		}
		</script>
	</div>
	<div class="row">	
		<div id="branch_data" class="form-group col-md-12">
		
		</div>
	</div>
</div>