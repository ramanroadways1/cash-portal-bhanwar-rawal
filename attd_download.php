<?php
require_once("./connect.php");

$status =array();

$m=$_POST['month'];
$y = $_POST['year'];
$branch = $_POST['branch'];

$number = cal_days_in_month(CAL_GREGORIAN, $m, $y); // 31

$date1 =$y."-0".$m."-01";
$date2 =$y."-0".$m."-".$number;
	?>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>

<div class="container-fluid">
<div class="row">
<div class="form-group col-md-12 table-responsive">
<table id="example" class="display" style="width:100%;font-family:Verdana;font-size:12px;">
    <thead>
	
	<tr>
		<th>Month/Year</th>	
		<th>Name</th>	
		<th>Branch</th>
		<th>Total P</th>
		<th>Total A</th>
		<th>Total HD</th>
		<?php for ($i=1; $i <= $number ; $i++) { 
			echo "<th>".$i."</th>";
		} ?>
	
	</tr>
	</thead>
        <tbody>
<?php
if($branch=='ALL')
{
$sql = mysqli_query($conn, "SELECT name,code,branch FROM emp_attendance ORDER BY id ASC");	
}
else
{
$sql = mysqli_query($conn, "SELECT name,code,branch FROM emp_attendance WHERE branch='$branch' ORDER BY id ASC");	
}

if(!$sql)
{
	echo mysqli_error($conn);
	exit();
}
while($row =mysqli_fetch_array($sql)){
	$code = $row['code'];
$sql1 = mysqli_query($conn, "SELECT status FROM emp_attendance_save WHERE date BETWEEN '$date1' AND '$date2' AND code='$code'");

while ($row2=mysqli_fetch_array($sql1)) {
	$data = $row2['status'];
	array_push($status, $data);
}
$count = count($status);
$p=array();
$a = array();
$hd=array();

for ($i=0; $i <$count ; $i++) { 
	$sign =$status[$i];
	if($sign == 'A'){
		array_push($a, 'A');
	}
	elseif ($sign == 'P') {
		array_push($p, 'P');
		# code...
	}
	elseif ($sign == 'HD') {
		array_push($hd, 'HD');
		# code...
	}
}
$countp = count($p);
$counta = count($a);
$counthd = count($hd);
?>
<tr>
	<td><?php echo $m."/".$y; ?></td>
	<td><?php echo $row['name']; ?></td>
	<td><?php echo $row['branch']; ?></td>
	<td><?php echo $countp; ?></td>
	<td><?php echo $counta; ?></td>
	<td><?php echo $counthd; ?></td>
	
	<?php for ($i=0; $i <$number ; $i++) { 
		if($i<=$count){
			echo "<td>".@$status[$i]."</td>";
		}
		else{
			echo "<td></td>";
		}
		} ?>
</tr>
<?php
$status = array();
}
?>
 </tbody>
</table>
</div>
</div>
</div>

<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script>