<?php
require_once '../connect.php'; 

$fid=mysqli_real_escape_string($conn,strtoupper($_POST['fids']));

$data = '<table class="table table-bordered table-striped" style="font-family:Verdana;font-size:11px;">
						<tr>    
							<th>LRNo</th>
							<th>From </th>
							<th>To</th>
							<th>Consignor</th>
							<th>Consignee</th>
							<th>Weight</th>
							<th>Rate</th>
							<th>ActFrt</th>
							<th>Total Frt</th>
							<th>Advance</th>
							<th>Balance</th>
						</tr>';

$query = "SELECT id,lrno,fstation,tstation,consignor,consignee,weight,ratepmt,actualf,loadd,newother,newtds,totalf,advamt,balamt FROM freight_form_lr where frno='$fid'";
$query2 = "select sum(weight),sum(actualf),sum(loadd),sum(newother),sum(newtds),sum(totalf),sum(advamt),sum(balamt) from freight_form_lr where frno='$fid'";

$result=mysqli_query($conn,$query);
$numrows=mysqli_num_rows($result);

$result2 = mysqli_query($conn,$query2);

$row2 = mysqli_fetch_array($result2);

	if(!$result)
	{
		mysqli_close($conn);
		mysqli_error($conn);
        exit();
    }
	
	if(!$result2)
	{
		mysqli_close($conn);
		mysqli_error($conn);
        exit();
    }
	
	if(mysqli_num_rows($result) > 0)
    {
    	$number = 1;
    	while($row = mysqli_fetch_assoc($result))
    	{
			$lrno_db=$row['lrno'];	
    		$data .= '<tr>
				<td>'.$row['lrno'].'</td>
				<td>'.$row['fstation'].'</td>
				<td>'.$row['tstation'].'</td>
				<td>'.$row['consignor'].'</td>
				<td>'.$row['consignee'].'</td>
				<td>'.$row['weight'].'</td>
				<td>'.$row['ratepmt'].'</td>	
		        <td>'.$row['actualf'].'</td>
		        <td>'.$row['totalf'].'</td>
		        <td>'.$row['advamt'].'</td>
		        <td>'.$row['balamt'].'</td>
            </tr>
                ';
    		$number++;
//<td class="hyd"><button onclick="GetUserDetails('.$row['id'].')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></button></td>							
    	}


 $data .= '<tr>
				<td colspan=5><b>The Total Calculation of Freight Form LR :</b></td>				
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2['sum(weight)']).'</b></td>
				<td></td>
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(actualf)"]).'</b></td>
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(totalf)"]).'</b></td>
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(advamt)"]).'</b></td>
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(balamt)"]).'</b></td>
		  </tr>
		  ';
	}
    else
    {
    	// records now found 
		// <td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(loadd)"]).'</b></td>
				// <td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(newother)"]).'</b></td>
				// <td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2["sum(newtds)"]).'</b></td>
				
    	$data .= '<tr><td colspan="16"><b>Records not found!</b></td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>