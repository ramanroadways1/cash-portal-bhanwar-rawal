<?php
require_once 'connect.php';
$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CASH PORTAL : RAMAN ROADWAYS PVT LTD</title>
<meta http-equiv="refresh" content="240">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

<style> 
 label{
	 font-family:Verdana;
	 font-size:14px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 

</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<form action="tds_sheet_download.php" method="POST" autocomplete="off">	

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	<br />
	<div class="row">
	
		<div class="form-group col-md-10 col-md-offset-1">
		<br />
		<br />
		<br />
		<div class="col-md-3">
			<div class="form-group">
                  <label class="control-label mb-1">From Date <font color="red"><sup>*</sup></font></label>
				  <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" name="from_date" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
               </div>
        </div>
		
		<div class="col-md-3">
			<div class="form-group">
                  <label class="control-label mb-1">To Date <font color="red"><sup>*</sup></font></label>
				  <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" name="to_date" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
               </div>
        </div>
												
		<div class="col-md-12">
                   <div class="form-group">
                     <button type="submit" id="button1" class="btn btn-danger">Download !</button>
                  </div>
          </div>
		</div>
		</div>
		
</div>
<br />
</div>
</div>

</body>
</html>