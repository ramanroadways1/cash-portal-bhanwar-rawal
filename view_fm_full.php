<?php				
require_once './connect.php';

$idmemo=mysqli_real_escape_string($conn,strtoupper($_POST['idmemo']));

$fetch_data=mysqli_query($conn,"SELECT f.newdate,f.truck_no,f.branch,f.company,f.actualf,
f.newtds,f.dsl_inc,f.newother,f.tds,f.totalf,f.totaladv,f.ptob,
f.adv_date,f.cashadv,f.chqadv,f.chqno,f.disadv,f.rtgsneftamt,f.narre,
f.baladv,f.unloadd,f.otherfr,f.claim,f.late_pod,f.totalbal,f.paidto,f.bal_date,
f.paycash,f.paycheq,f.paycheqno,f.paydsl,f.newrtgsamt,f.narra,f.sign_cash,f.sign_adv,f.sign_bal,f.pod_date,f.branch_bal,
b.name as b_name,b.mo1 as b_mobile,b.pan as b_pan,
d.name as d_name,d.mo1 as d_mobile,d.pan as d_lic,
o.name as o_name,o.mo1 as o_mobile,o.pan as o_pan,
pod.branch as pod_branch,
dsl.dsl_by as adv_dsl_by,dsl.dcard as adv_card,dsl.dsl_nrr as adv_dsl_nrr,
dsl1.dsl_by as bal_dsl_by,dsl1.dcard as bal_card,dsl1.dsl_nrr as bal_dsl_nrr 
FROM freight_form as f
		LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
		LEFT OUTER JOIN mk_driver AS d ON d.id = f.did
		LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
		LEFT OUTER JOIN rcv_pod AS pod ON pod.frno = f.frno
		LEFT OUTER JOIN diesel_fm AS dsl ON dsl.fno = f.frno AND dsl.type='ADVANCE'
		LEFT OUTER JOIN diesel_fm AS dsl1 ON dsl1.fno = f.frno AND dsl1.type='BALANCE'
		WHERE f.frno='$idmemo'");

if(!$fetch_data)
{
	echo mysqli_error($conn);
	exit();
}
	if(mysqli_num_rows($fetch_data)==0)
	{
		echo "No result found..";
		exit();
	}
	
		$row1=mysqli_fetch_array($fetch_data);
		
		$fmdt=$row1['newdate'];
		$tno=$row1['truck_no'];
		$fm_branch=$row1['branch'];
		$company=$row1['company'];
		$af=$row1['actualf'];
		$load=$row1['newtds'];
		$dsl_inc=$row1['dsl_inc'];
		$other=$row1['newother'];
		$tds=$row1['tds'];
		$tf=$row1['totalf'];
		$total_adv=$row1['totaladv'];
		$adv_to=$row1['ptob'];
		$adv_date=$row1['adv_date'];
		$cash=$row1['cashadv'];
		$cheque=$row1['chqadv'];
		$cheque_no=$row1['chqno'];
		$diesel=$row1['disadv'];
		$rtgs=$row1['rtgsneftamt'];
		$adv_narr=$row1['narre'];
		
		$balance=$row1['baladv'];
		$unloading=$row1['unloadd'];
		$other_charge=$row1['otherfr'];
		$claim=$row1['claim'];
		$late_pod=$row1['late_pod'];
		$total_balance=$row1['totalbal'];
		$bal_to=$row1['paidto'];
		$bal_date=$row1['bal_date'];
		$cash2=$row1['paycash'];
		$cheque2=$row1['paycheq'];
		$cheque_no2=$row1['paycheqno'];
		$diesel2=$row1['paydsl'];
		$rtgs2=$row1['newrtgsamt'];
		$bal_narr=$row1['narra'];
		
		$sign_cash=$row1['sign_cash'];
		$sign_adv=$row1['sign_adv'];
		$sign_bal=$row1['sign_bal'];
		$pod_date=$row1['pod_date'];
		$branch_bal=$row1['branch_bal'];
		
		$broker_name=$row1['b_name'];
		$broker_mobile=$row1['b_mobile'];
		$broker_pan=$row1['b_pan'];
		
		$drover_name=$row1['d_name'];
		$driver_mobile=$row1['d_mobile'];
		$driver_lic=$row1['d_lic'];
		
		$owner_name=$row1['o_name'];
		$owner_mobile=$row1['o_mobile'];
		$owner_pan=$row1['o_pan'];
		$pod_by=$row1['pod_branch'];
		
$data_table='<table class="table table-bordered" style="margin-top:5px;font-family:Verdana;font-size:11px">

			<tr>   
                        <th>Act Frt</th>
						<th>Load</th>
						<th>Dsl Inc</th>
						<th>Other</th>
						<th>TDS</th>
						<th>Total Frt</th>
						<th>Adv to</th>
						<th>Cash</th>
						<th>Cheq</th>
						<th>Diesel</th>
						<th>RTGS/NEFT</th>
						<th>Total Adv</th>
						<th>Balance</th>
                        <th>Narr.</th>
</tr>
<tr>
				<td>'.$row1['actualf'].'</td>
				<td>'.$row1['newtds'].'</td>
				<td>'.$row1['dsl_inc'].'</td>
				<td>'.$row1['newother'].'</td>
				<td>'.$row1['tds'].'</td>
				<td>'.$row1['totalf'].'</td>
				<td>'.$row1['ptob'].'</td>
				<td>'.$row1['cashadv'].'</td>
				<td>'.$row1['chqadv'].'</td>
				<td>'.$row1['disadv'].'</td>
				<td>'.$row1['rtgsneftamt'].'</td>
				<td>'.$row1['totaladv'].'</td>
				<td>'.$row1['baladv'].'</td>
                <td>'.$row1['narre'].'</td>
	</tr>
</table>	
';		

$data_table_bal='<table class="table table-bordered" style="margin-top:5px;font-family:Verdana;font-size:11px">

			<tr>   
                 <th>Balance</th> <td>'.$row1['baladv'].'</td>
				 <th>Unloading/Detn.</th> <td>'.$row1['unloadd'].'</td>
				 <th>Other</th><td>'.$row1['otherfr'].'</td>
				<th>Claim</th><td>'.$row1['claim'].'</td>
				<th>LatePOD</th><td>'.$row1['late_pod'].'</td>
				<th>TotalBal</th><td>'.$row1['totalbal'].'</td>
				<th>Bal To</th><td>'.$row1['paidto'].'</td>
			</tr>
			<tr>
						<th>Cash</th><td>'.$row1['paycash'].'</td>			
						<th>Cheque</th><td>'.$row1['paycheq'].'</td>
						<th>ChequeNo</th><td colspan="2">'.$row1['paycheqno'].'</td>
						<th>Diesel</th><td>'.$row1['paydsl'].'</td>
						<th>RTGS/NEFT</th><td>'.$row1['newrtgsamt'].'</td>
						<th>Balance Date</th><td colspan="2">'.$row1['bal_date'].'</td>
			</tr>
			<tr>
			<th>POD RcvdBy</th><td colspan="2">'.$row1['pod_branch'].' ('.$pod_date.')'.'</td>
						<th>Balance By</th> <td>'.$row1['branch_bal'].'</td>
                        <th>Narr.</th><td colspan="8">'.$row1['narra'].'</td>
			</tr>
		
</table>	
';		

if($row1['disadv']>0)
{
$diesel_table='
<span style="color:blue;font-size:13px;font-weight:bold">Diesel Details -</span>
	<div style="overflow-x:auto;" class="table-responsive">
<table class="table table-bordered" style="margin-top:5px;font-family:Verdana;font-size:11px">

			<tr>   
                        <th>Card/Pump</th>
						<th>Card No/Pump Code</th>
						<th>Diesel Narration</th>
			</tr>
			<tr>
				<td>'.$row1['adv_dsl_by'].'</td>
				<td>'.$row1['adv_card'].'</td>
				<td>'.$row1['adv_card'].'</td>
			</tr>
</table>
</div>	
';
}
else
{
	$diesel_table='';
}

if($row1['paydsl']>0)
{
$diesel_table_bal='
<span style="color:blue;font-size:13px;font-weight:bold">Bal Diesel Details -</span>
	<div style="overflow-x:auto;" class="table-responsive">
<table class="table table-bordered" style="margin-top:5px;font-family:Verdana;font-size:11px">

			<tr>   
                        <th>Card/Pump</th>
						<th>Card No/Pump Code</th>
						<th>Diesel Narration</th>
			</tr>
			<tr>
				<td>'.$row1['bal_dsl_by'].'</td>
				<td>'.$row1['bal_card'].'</td>
				<td>'.$row1['bal_dsl_nrr'].'</td>
			</tr>
</table>
</div>	
';
}
else
{
	$diesel_table_bal='';
}
		
		$fmdate=date('d/m/y', strtotime($fmdt));
		$adv_date1=date('d/m/y', strtotime($adv_date));
		$bal_date1=date('d/m/y', strtotime($bal_date));
?>
<!DOCTYPE html>
<html lang="en">
<head>

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center>
	<img style="margin-top:150px" src="./load.gif" />
</center>
</div>

<style>
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}

.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>	

<style type="text/css">
@media print
{
body {
   zoom:70%;
 }	
body * { visibility: hidden; }
#printpage * { visibility: visible; }
#printpage { position: absolute; top: 0; left: 0; }
}
</style>

</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<form action='./vou_print_bal.php' target="_blank" id='myForm22' method='POST'>
	<input type='hidden' value="<?php echo $idmemo ?>" name="frno">		
</form>
		
<script>
function VouPrint2(frno)
{
			function submitform()
			{
				document.getElementById('myForm22').submit();
			}
		submitform();	
}
</script>

<body style="overflow-x: scroll !important;" onload="lrFetch();">

<a href="./">
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;color:#FFF;font-family:Verdana" class="btn btn-primary">Dashboard</button></a>
	<button style="font-family:Verdana;margin-top:10px;margin-left:10px;font-weight:bold;" class="btn btn-danger" onclick="print()">Print Freight Memo</button>
	<button onclick="VouPrint2('<?php echo strtoupper($idmemo); ?>')" style="font-family:Verdana;margin-top:10px;margin-left:10px;font-weight:bold;" class="btn btn-danger">Print Vouchers</button>
<div id="printpage">
 <div class="container-fluid">
	<div class="row" style="font-family:Verdana">
	  <div class="col-md-3">
		
	</div>		
	 <div class="col-md-6">
		<center><span class="" style="font-size:15px;letter-spacing:1px">Freight Memo : <?php echo strtoupper($idmemo); ?> </span> </center>
	</div>		
	 <div class="col-md-3">
		<span style="font-weight:bold;font-size:12px;margin-right:10px" class="pull-right">FM Date : <?php echo $fmdate; ?>, Adv Date : <?php echo $adv_date; ?></span>
	</div>
 </div>

<input type="text" id="fids" name="fids" value="<?php echo $idmemo; ?>" style="visibility:hidden;" />

<div class="row">
<div class="col-md-12" style="font-family: verdana;">
<table border="0" width="100%" style="font-size:11px;">
	<tr>
		<td>
           <label>Truck No: &nbsp;</label>
           <?php echo $tno; ?>
        </td>
		
		<td>
           <label>Company: &nbsp;</label>
           <?php echo $company; ?>
        </td>
        
		<td>  
			<label>Branch: &nbsp;</label>
           <?php echo $fm_branch; ?>
        </td>
	</tr>		

	<tr>
        <td>   
			<label>Broker: &nbsp;</label>
           <?php echo $broker_name; ?>
        </td>
		
		<td>  
           <label>Broker Mo.: &nbsp;</label>
           <?php echo $broker_mobile; ?>
        </td>
		
		<td>  
           <label>Broker PAN: &nbsp;</label>
           <?php echo $broker_pan; ?>
        </td>
</tr>		

<tr>
		<td>
           <label>Driver: &nbsp;</label>
           <?php echo $drover_name; ?>
        </td>
        
		<td>  
           <label>Driver Mo.: &nbsp;</label>
           <?php echo $driver_mobile; ?>
		</td>
        
		<td>  
			<label>Driver LIC: &nbsp;</label>
           <?php echo $driver_lic; ?>
        </td>
</tr>		

<tr>
		<td>
			<label>Owner: &nbsp;</label>
           <?php echo $owner_name; ?>
        </td>
		
		<td>  
           <label>Owner Mobile: &nbsp;</label>
           <?php echo $owner_mobile; ?>
        </td>
		
		<td>  
           <label>Owner PAN: &nbsp;</label>
           <?php echo $owner_pan; ?>
        </td>
</tr>		
</table>
<br />

<div class="row" id="data_table_div">
	<div class="col-md-12" style="font-family:Verdana;color:#000; font-size:11px;">
	<span style="color:blue;font-size:12px;font-weight:bold">Advance Details -</span>
		<div style="overflow-x:auto;" class="table-responsive"><?php echo $data_table; ?></div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="font-family: Verdana; color:#000; font-size:12px;">
	<?php echo $diesel_table; ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="font-family: Verdana; color:#000; font-size:12px;">
	<?php echo $diesel_table_bal; ?>
	</div>
</div>

<div class="row" id="data_table_div">
	<div class="col-md-12" style="font-family:Verdana;color:#000; font-size:11px;">
	<span style="color:blue;font-size:12px;font-weight:bold">Balance Details -</span>
		<div style="overflow-x:auto;" class="table-responsive"><?php echo $data_table_bal; ?></div>
	</div>
</div>

        </div></div>
		</div>
<script type="text/javascript">
lrFetch();
function lrFetch()
{ 
	$("#new").show();
	jQuery.ajax({
	url: "./ajax/readRecords.php",
	data: 'fids=' + $("#fids").val(),
	type: "POST",
	success: function(data) {
	$(".records_content").html(data);
	$("#new").hide();
	},
	error: function() {}
	});
}					
</script>	
	
		<div class="container-fluid">
<span style="color:blue;font-family:Verdana;font-size:12px;font-weight:bold">Freight Memo LR :</span>
        <div class="records_content table-responsive" style="font-family:Verdana; color:#000; margin-top:5px;font-size:12px"></div>

<table border="0" style="width:100%">
	<tr>
		<td align="center"><label style="font-size:13px;">Cashier Sign</label></td>
		<td align="center"><label style="font-size:13px;">Adv Rcvr</label></td>
		<td align="center"><label style="font-size:13px;">Bal Rcvr</label></td>
	</tr>	
	<tr>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../<?php echo $sign_cash; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../<?php echo $sign_adv; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../<?php echo $sign_bal; ?>"></td>
	</tr>	
</table>
</div> 
</div>
	
</body>
</html>